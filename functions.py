def lst_length():
    lst = [70, 80, 90]
    print("new lst length is : ", len(lst))


def tup_length():
    tup = (40, 50, 60)
    print("tup length is : ", len(tup))


def set_length():
    sett = {70, 80, 90}
    print("new sett length is : ", len(sett))


def lst_comprehension():
    lst = (40, 50, 60)
    lst_new = [i * 5 for i in lst]
